package eu.tsoml;

import eu.tsoml.data.DataFactory;
import eu.tsoml.model.Store;

import java.util.Scanner;

public class CatalogApp {

    private static Store[] stores = DataFactory.generate();

    public static void main(String[] args) {

        findCheapestPhone();
        findByBrand();
        findByModel();

    }


    public static void findCheapestPhone() {
        System.out.println("Task 1");
        int cheapestStoreID = 0;
        int cheapestPrice = stores[cheapestStoreID].getCheapestPrice();
        for (int i = 1; i < stores.length; i++) {
            if (stores[i].getCheapestPrice() < stores[cheapestStoreID].getCheapestPrice()) {
                cheapestStoreID = i;
                cheapestPrice = stores[i].getCheapestPrice();
            }
        }
        stores[cheapestStoreID].findByPrice(cheapestPrice);

    }

    public static void findByBrand() {
        System.out.println("\nTask 2");
        System.out.print("Enter brand name: ");
        Scanner scanner = new Scanner(System.in);
        String brand = scanner.nextLine();

        for (Store store : stores) {
            store.findByBrand(brand);
        }

    }

    public static void findByModel() {
        System.out.println("\nTask 3");
        System.out.print("Enter model name: ");
        Scanner scanner = new Scanner(System.in);
        String model = scanner.nextLine();

        int cheapestPrice = 0;
        int cheapestStoreID = 0;

        for (Store store : stores) {
            if (store.isModelAvailable(model)) {
                cheapestPrice = store.getPriceByModel(model);
                break;
            }
        }

        for (int i = 0; i < stores.length; i++) {
            if (stores[i].getPriceByModel(model) < cheapestPrice) {
                cheapestPrice = stores[i].getPriceByModel(model);
                cheapestStoreID = i;
            }

            if (i == stores.length - 1){
                stores[cheapestStoreID].findByModel(model);
            }
        }
    }
}
