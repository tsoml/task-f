package eu.tsoml.model;

public class Smartphone {

    private String brand;
    private String model;
    private int priceUSD;

    public Smartphone(String brand, String model, int price) {
        this.brand = brand;
        this.model = model;
        this.priceUSD = price;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getPriceUSD() {
        return priceUSD;
    }



}
