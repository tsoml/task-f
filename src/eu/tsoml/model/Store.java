package eu.tsoml.model;

public class Store {

    private String name;
    private String address;
    private Smartphone[] smartphones;

    public Store(String name, String address, Smartphone[] smartphones) {
        this.name = name;
        this.address = address;
        this.smartphones = smartphones;
    }


    public boolean isModelAvailable(String model) {
        for (Smartphone smartphone : smartphones) {
            return smartphone.getModel().equalsIgnoreCase(model);
        }
        return false;
    }

    public int getCheapestPrice() {
        int cheapestPhoneId = 0;
        for (int i = 1; i < smartphones.length; i++) {
            if (smartphones[i].getPriceUSD() < smartphones[cheapestPhoneId].getPriceUSD()) {
                cheapestPhoneId = i;
            }
        }
        return smartphones[cheapestPhoneId].getPriceUSD();
    }

    public Integer getPriceByModel(String model) {

        for (Smartphone smartphone : smartphones) {
            if (smartphone.getModel().equalsIgnoreCase(model)) {
                return smartphone.getPriceUSD();
            }
        }
        return null;
    }


    public void findByPrice(int price) {
        for (Smartphone smartphone : smartphones) {
            if (smartphone.getPriceUSD() == price) {
                System.out.println("Brand: " + smartphone.getBrand() +
                        ", model: " + smartphone.getModel() +
                        ", price: " + smartphone.getPriceUSD() +
                        ". Store: " + name +
                        ", address: " + address);
            }
        }
    }

    public void findByBrand(String brand) {
        for (Smartphone smartphone : smartphones) {
            if (smartphone.getBrand().equalsIgnoreCase(brand)) {
                System.out.println("Brand: " + smartphone.getBrand() +
                        ", model: " + smartphone.getModel() +
                        ", price: " + smartphone.getPriceUSD() +
                        ". Store: " + name +
                        ", address: " + address);
            }
        }
    }

    public void findByModel(String model) {
        for (Smartphone smartphone : smartphones) {
            if (smartphone.getModel().equalsIgnoreCase(model)) {
                System.out.println("Brand: " + smartphone.getBrand() +
                        ", model: " + smartphone.getModel() +
                        ", price: " + smartphone.getPriceUSD() +
                        ". Store: " + name +
                        ", address: " + address);
            }
        }
    }

}
