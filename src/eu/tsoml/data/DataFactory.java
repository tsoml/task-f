package eu.tsoml.data;

import eu.tsoml.model.Smartphone;
import eu.tsoml.model.Store;

public class DataFactory {

    public static Store[] generate() {

        Store[] stores = new Store[2];

        {
            Smartphone[] smartphones = {
                    new Smartphone("Google", "Pixel 4", 700),
                    new Smartphone("OnePlus", "7T", 500),
                    new Smartphone("Xiaomi", "Mi 10", 600),
                    new Smartphone("Apple", "Iphone 11", 650),
                    new Smartphone("Apple", "Iphone 11 PRO", 800)

            };

            stores[0] = new Store("Cactus", "Kharkov, Pavlovskaya Square 8", smartphones);
        }


        {
            Smartphone[] smartphones = {
                    new Smartphone("Google", "Pixel 4", 750),
                    new Smartphone("Samsung", "S10", 500),
                    new Smartphone("Samsung", "S20", 750),
                    new Smartphone("Apple", "Iphone 11", 700),
                    new Smartphone("Apple", "Iphone 11 PRO", 850)

            };

            stores[1] = new Store("Citrus", "Sumska St, 17", smartphones);
        }


        return stores;
    }

}
